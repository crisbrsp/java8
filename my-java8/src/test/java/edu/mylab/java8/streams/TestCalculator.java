package edu.mylab.java8.streams;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests {@link Calculator}.
 *
 * @author crisbrsp
 */
public class TestCalculator
{
    /**
     * Tests that, for a given list of numbers, both imperative
     * and declarative returns the same correct result.
     */
    @Test
    public void testTotal()
    {
        List<Long> numbers = Arrays.asList(4L, 5L, 10L, 23L, 48L, 2L, 59L);

        Calculator calculator = new Calculator();

        assertEquals(new Long(140L), calculator.imperativeTotal(numbers));
        assertEquals(new Long(140L), calculator.functionalTotal(numbers));
    }
}
