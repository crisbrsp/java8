package edu.mylab.java8.datetime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

/**
 * Tests {@link UTCConverter}.
 *
 * @author crisbrsp
 */
public class TestUTCConverter
{
    private static final ZoneId SP = ZoneId.of("America/Sao_Paulo");

    private static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * Tests that the current date time (now) in Sao Paulo is
     * properly converted to its equivalent in UTC.
     */
    @Test
    public void testConvertNow()
    {
        // "now" in SP
        ZonedDateTime nowInSP = ZonedDateTime.now(SP);

        // expected "now" in UTC
        // the seconds is adjusted to guarantee the same one from equivalent in SP
        ZonedDateTime nowUTC = ZonedDateTime.now(ZoneOffset.UTC).withSecond(nowInSP.getSecond());

        // convert
        ZonedDateTime convertedNow = UTCConverter.INSTANCE.convert(nowInSP);

        // assert conversion
        assertNotNull(convertedNow);
        assertEquals(nowUTC.format(DATE_PATTERN), convertedNow.format(DATE_PATTERN));
    }

    /**
     * Tests that some date time in Sao Paulo is properly
     * converted to its equivalent in UTC.
     */
    @Test
    public void testConvertSomeDateTime()
    {
        // create some day
        LocalDate someDay = LocalDate.of(2015, 10, 2);

        // create some time in SP
        LocalTime someTimeInSP = LocalTime.of(22, 10, 5);

        // create time in UTC, equivalent to the one in SP
        LocalTime someTimeUTC = LocalTime.of(1, 10, 5);

        // expected UTC date time: in this case, it will be the next day!
        ZonedDateTime dateTimeUTC = ZonedDateTime.of(someDay.plusDays(1), someTimeUTC, ZoneId.of("UTC"));

        // zoned input date time
        ZonedDateTime dateTimeInSP = ZonedDateTime.of(someDay, someTimeInSP, SP);

        // convert
        ZonedDateTime convertedDateTime = UTCConverter.INSTANCE.convert(dateTimeInSP);

        // assert conversion
        assertNotNull(convertedDateTime);
        assertEquals(dateTimeUTC.format(DATE_PATTERN), convertedDateTime.format(DATE_PATTERN));
    }

    /**
     * Tests that the same given UTC date time is properly
     * returned from the conversion process.
     */
    @Test
    public void testConvertUTCDateTime()
    {
        // create date time UTC
        LocalDate someDay = LocalDate.of(2015, 10, 2);
        LocalTime someTime = LocalTime.of(7, 10, 5);

        ZonedDateTime dateTimeUTC = ZonedDateTime.of(someDay, someTime, ZoneId.of("UTC"));

        // convert
        ZonedDateTime convertedDateTime = UTCConverter.INSTANCE.convert(dateTimeUTC);

        // assert conversion
        assertNotNull(convertedDateTime);
        assertEquals(dateTimeUTC.format(DATE_PATTERN), convertedDateTime.format(DATE_PATTERN));
    }
}
