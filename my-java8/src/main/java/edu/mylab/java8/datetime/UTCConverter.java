package edu.mylab.java8.datetime;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * Singleton that provides an operation to convert a given
 * {@link ZonedDateTime} to its equivalent in UTC.
 *
 * @author crisbrsp
 */
enum UTCConverter
{
    INSTANCE;

    /**
     * Receives a {@link ZonedDateTime}, and converts it to
     * the equivalent instant in UTC.
     *
     * @param pValue {@link ZonedDateTime} to be converted to UTC
     *
     * @return {@link ZonedDateTime} converted to UTC
     */
    ZonedDateTime convert(ZonedDateTime pValue)
    {
        if (ZoneOffset.UTC.equals(pValue.getOffset()))
        {
            return pValue;
        }

        return pValue.withZoneSameInstant(ZoneId.of("UTC"));
    }
}
