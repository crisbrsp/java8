package edu.mylab.java8.datetime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Playing around with Java 8 Date Time API, by
 * checking the handling of one same instant in
 * different time zones.
 *
 * @author crisbrsp
 */
public class PlayingWithPointInTime
{
    private static final ZoneId SP = ZoneId.of("America/Sao_Paulo");

    public static void main(String[] args)
    {
        // 2015-07-25T22:10:05-03:00 in Sao Paulo
        LocalDate day = LocalDate.of(2015, 07, 25);
        LocalTime time = LocalTime.of(22, 10, 5);
        ZonedDateTime dateTimeInSP = ZonedDateTime.of(day, time, SP);

        System.out.println(dateTimeInSP);

        // take the moment as an UTC instant: 2015-07-26T01:10:05Z
        System.out.println(dateTimeInSP.toInstant());

        // take the same moment, in Tokyo: 2015-07-26T10:10:05+09:00
        System.out.println(dateTimeInSP.toInstant().atZone(ZoneId.of("Asia/Tokyo")));

        // Is DST (daylight saving time)? In this case, no.
        System.out.println(SP.getRules().isDaylightSavings(dateTimeInSP.toInstant()));

        // But it should be DST in 2015-10-25T22:10:05-03:00 in Sao Paulo
        System.out.println(SP.getRules().isDaylightSavings(dateTimeInSP.plusMonths(3).toInstant()));
    }
}
