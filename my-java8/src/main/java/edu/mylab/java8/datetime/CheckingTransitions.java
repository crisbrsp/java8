package edu.mylab.java8.datetime;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.zone.ZoneOffsetTransition;

/**
 * Playing around with Java 8 Date Time API, by
 * checking the transitions available in one time-zone ID,
 * for the current and next years.
 *
 * Transitions are used to determine DST (daylight saving time).
 *
 * @author crisbrsp
 */
public class CheckingTransitions
{
    public static void main(String[] args)
    {
        displayZoneTransitions(ZoneId.of("America/Sao_Paulo"));
        displayZoneTransitions(ZonedDateTime.now().getZone());
    }

    /**
     * Displays the transitions inside of a time-zone ID.
     *
     * @param pZoneId Instance of {@link ZoneId} to be considered
     */
    private static void displayZoneTransitions(ZoneId pZoneId)
    {
        System.out.println(pZoneId);

        for (ZoneOffsetTransition transition : pZoneId.getRules().getTransitions())
        {
            ZonedDateTime transitionDateTime = transition.getInstant().atZone(pZoneId);

            if (showTransition(transitionDateTime))
            {
                System.out.println(transition);
            }
        }
    }

    /**
     * Determines if the year of the given {@link ZonedDateTime} is the current
     * one, or the next year.
     *
     * @param transitionDateTime Instance of {@link ZonedDateTime} to be evaluated
     *
     * @return True, if current or next year. False, otherwise.
     */
    private static boolean showTransition(ZonedDateTime transitionDateTime)
    {
        int currentYear = ZonedDateTime.now().getYear();
        int nextYear = currentYear + 1;

        return transitionDateTime.getYear() == currentYear || transitionDateTime.getYear() == nextYear;
    }
}
