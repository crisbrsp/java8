package edu.mylab.java8.datetime;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * Playing around with Java 8 Date Time API, by
 * checking different types of "now".
 *
 * @author crisbrsp
 */
class PlayingWithNow
{
    public static void main(String[] args)
    {
        // no offset or time zone
        LocalDateTime localNow = LocalDateTime.now();
        System.out.println(localNow);

        // with offset and time zone
        ZonedDateTime zonedNow = ZonedDateTime.now();
        System.out.println(zonedNow);

        // UTC
        ZonedDateTime utcNow = ZonedDateTime.now(ZoneOffset.UTC);
        System.out.println(utcNow);

        // with Sao Paulo's offset and time zone
        ZonedDateTime spNow = ZonedDateTime.now(ZoneId.of("America/Sao_Paulo"));
        System.out.println(spNow);

        // just now
        Instant now = Instant.now();
        System.out.println(now);
        System.out.println(now.atOffset(ZoneOffset.of("+05:00")));
        System.out.println(now.atZone(ZoneId.of("Asia/Tokyo")));
    }
}
