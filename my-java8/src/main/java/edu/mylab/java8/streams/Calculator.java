package edu.mylab.java8.streams;

import java.util.List;

/**
 * Simple calculator class to compare usage of
 * imperative and declarative programming approaches.
 *
 * @author crisbrsp
 */
public class Calculator
{
    /**
     * Filtering and summing up the totals in a imperative style.
     *
     * @param pNumbers {@link List<Long>} containing some numbers
     * @return Sum of numbers bigger or equal than 10
     */
    Long imperativeTotal(List<Long> pNumbers)
    {
        // creating local variable
        Long total = 0L;

        // conveying the intent: FOR and filtering
        for (Long number : pNumbers)
        {
            if (number >= 10)
            {
                // reassignment of variables
                total += number;
            }
        }

        return total;
    }

    /**
     * Filtering and summing up the totals in a declarative style.
     *
     * @param pNumbers {@link List<Long>} containing some numbers
     * @return Sum of numbers bigger or equal than 10
     */
    Long functionalTotal(List<Long> pNumbers)
    {
        // less mutability: no reassignment of local variables

        // more concise = less code to write/read/maintain
        // the intention is effectively conveyed without too much code
        return pNumbers.stream()
                       .filter(n -> n >= 10)
                       .reduce(new Long(0), Long::sum); //it could also be here: (t, u) -> t + u
    }
}
